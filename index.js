const express = require("express");
/*
	is an ODM libraray to let our Express JS API
	to manipulate a MongoDB 
*/
const mongoose = require("mongoose");
const app = express();

const port = 4000;

//Mongoose Connection
mongoose.connect("mongodb+srv://admin:admin123@cluster0.hfsrf.mongodb.net/bookingAPI?retryWrites=true&w=majority",

		{
			useNewUrlParser: true,
			useUnifiedTopology: true
		}

	);

// checkt he connection if success.
let db = mongoose.connection;
// This is to show notification of an internal server error from MongoDB.
db.on('error', console.error.bind(console, "MongoDB Connection Error."));

// If the connection is open and successful, we will output a message in the terminal/gitbash.
db.once('open',()=>console.log("Connected to MongoDB"));



// to able to handle the request body and parse it
app.use(express.json());


// import our routes and use it as middleware
const courseRoutes = require('./routes/courseRoutes');
// console.log(courseRoutes);
app.use('/courses', courseRoutes);

const userRoutes = require('./routes/userRoutes');
app.use('/users', userRoutes);

app.listen(port,() => console.log(`Express API running at port ${4000}`));
/*
To able to create a  route from another file that will be used in our application
we have to impport express as well.
Ise Router() method here.

*/

const express = require("express");
const router = express.Router();

const courseControllers = require("../controllers/courseControllers");
const auth = require("../auth");
const {verify, verifyAdmin} = auth;

router.get('/', verify, verifyAdmin, courseControllers.getAllCourses);

router.post('/', verify, verifyAdmin, courseControllers.addCourse);



// get all active courses

router.get("/activeCourses", courseControllers.getActiveCourses);


// we can pass data in a route w/out the use of req.body by passing a small amount of
// data through the url with the use of route params
// http://localhost:4000/courses/getSingleCourse/62e8ca3f1abee1ececc40194
router.get("/getSingleCourse/:courseId", courseControllers.getSingleCourse);

// update a single course
// pass the id via route params, however the update details will be passed via req.body

router.put("/updateCourse/:courseId", verify, verifyAdmin, courseControllers.updateCourse);

// archive a single course or soft delete
router.delete("/archiveCourse/:courseId", verify, verifyAdmin, courseControllers.archiveCourse);

module.exports = router;
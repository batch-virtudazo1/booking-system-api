const express = require("express");
const router = express.Router();

router.get('/',(req,res)=>{
	res.send("This route will add new user");
});

const userControllers = require("../controllers/userControllers");
//console.log(userControllers);

const auth = require("../auth");
const {verify} = auth;

router.post('/', userControllers.registerUser);
router.get('/details', verify, userControllers.getUserDetails)


// Route for User Authentication

router.post('/login', userControllers.loginUser);

router.post('/checkEmail', userControllers.checkUserEmail);

// enrollment
router.post("/enroll", verify, userControllers.enroll);

module.exports = router;
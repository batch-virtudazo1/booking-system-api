/*
	auth.js is our own module which will contains methods to authorize or restrict users from accessing
	certain features in our applications

*/

const jwt = require("jsonwebtoken");

// this is the secret string which will validate or which will use to check the
// validity of a passed token. If the token does not contain this secret string, then
// that token is invalid or illigetimate.
const secret = "courseBookingAPI";

/*
	JWT is a way to pass informtaion from one server to frontend or other parts of the applciation
	that will allow an access.
*/

module.exports.createAccessToken = (userDetails) =>{

	//console.log(userDetails);

	const data = {
		id: userDetails.id,
		email: userDetails.email,
		isAdmin: userDetails.isAdmin
	}
	console.log(data);
	// .sign() will create a JWT using our data object with our secret
	return jwt.sign(data,secret,{});
}


module.exports.verify = (req,res,next) =>{
	let token = req.headers.authorization
	if(typeof token === "undefined"){
		return res.send({auth: "Failed. No token."});
	} else {
		
		// console.log(token);
		token = token.slice(7);
		// console.log(token);


		jwt.verify(token, secret, function(err,decodedToken){

			console.log(decodedToken);
			console.log(err);

			if(err){
				return res.send({
					auth: "Failed",
					message: err.message
				})
			} else{
				req.user = decodedToken;

				next();
			}
		})
	}

}
// verify admin

module.exports.verifyAdmin = (req,res,next) => {
		console.log(req.user);

		if(req.user.isAdmin){
			next();
		} else {
			return res.send({

				auth: "failed",
				message: "Action Forbidden"
			})
		}
}
/*
Capitalized name and singular name
*/

const mongoose = require("mongoose");

/*
	Mongoose Schema 

	Schema acts as our blueprint of our data/document. It is a representation of how the document is structured
		And determine the types of data and expected properties.

	Before we can create documents from our API to save into our database:
		>> Determine the structure of the documents first.
		>> Use Scheman() constrcutor.
*/

const userSchema = new mongoose.Schema({


	firstName: {

		type: String,
		required: [true, "first name is required"]
	},
	lastName: {

		type: String,
		required: [true, "Last name is required"]
	},
	email: {
		type: String,
		required: [true, "Email is required"]
	},
	password: {
		type: String,
		required: [true, "Password is required"]
	},
	mobileNo: {
		type: String,
		required: [true, "Mobile number is required"]
	},
	isAdmin: {
		type: Boolean,
		default: false
	},
	enrollments: [
		{
			courseId: {
				type: String,
				required: [true, "Course ID is required"]
			},
			status: {
				type: String,
				default: "Enrolled"
			},
			dateEnrolled: {
				type: Date,
				default: new Date()
			}
		}
	]


});

// use module.exports to import and use this file in another files.
/*
	Mongoose Model is our connection to our collection
	2 arguments:
	1st - name of the colelction
	2nd - it is the schema of the document in the collection

*/
module.exports = mongoose.model("User",userSchema);
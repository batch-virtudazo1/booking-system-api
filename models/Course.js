/*
Capitalized name and singular name
*/

const mongoose = require("mongoose");

/*
	Mongoose Schema 

	Schema acts as our blueprint of our data/document. It is a representation of how the document is structured
		And determine the types of data and expected properties.

	Before we can create documents from our API to save into our database:
		>> Determine the structure of the documents first.
		>> Use Scheman() constrcutor.
*/

const courseSchema = new mongoose.Schema({


	name: {

		type: String,
		required: [true, "Name is required"]
	},
	description: {
		type: String,
		required: [true, "Description is required"]
	},
	price: {
		type: Number,
		required: [true, "Price is required"]
	},
	isActive: {
		type: Boolean,
		default: true
	},
	createdOn: {
		type: Date,
		default: new Date()
	},
	enrollees: [
		{
			userId : {
				type: String,
				required: [true, "User Id is required"]
			},
			dateEnrolled: {
				type: Date,
				default: new Date()
			},
			status: {
				type: String,
				default: "Enrolled"
			}
		}

	]


});

// use module.exports to import and use this file in another files.
/*
	Mongoose Model is our connection to our collection
	2 arguments:
	1st - name of the colelction
	2nd - it is the schema of the document in the collection

*/
module.exports = mongoose.model("Course",courseSchema);
const Course = require("../models/Course");

module.exports.getAllCourses = (req,res)=>{

	// in mongoDb: db.courses.find({})
	// Model.find() returns a collection that macthes our criteria
	// to get all the courses we have
	Course.find({})
	.then(result => res.send(result))
	.catch(error => res.send(error))


	// res.send("This route will get all courses documents");
}

module.exports.addCourse = (req,res)=>{

	// console.log(req.body);
	// res.send("This route will create a new course document");
	
	// using the course model we will use its constructor to create our
	// course document w/c will follow the schemas fo the model
	let newCourse = new Course({

		name: req.body.name,
		description: req.body.description,
		price: req.body.price

	})


	//console.log(newCourse);

	newCourse.save()
	.then(result => res.send(result))
	.catch(error => res.send(error))

}

module.exports.getActiveCourses = (req,res) => {

	Course.find({isActive:true})
	.then(result => res.send(result))
	.catch(error => res.send(error))
}


module.exports.getSingleCourse = (req, res) => {
	// req.params is an object that contains the value of req.body via a route params
	// the field name of the  req.params indicate the name of the route params.
	
	/*console.log(req.params);
	console.log(req.params.courseId);*/

	Course.findById(req.params.courseId)
	.then(result => res.send(result))
	.catch(error => res.send(error))

}
	module.exports.updateCourse   = (req, res) => {
		// checking if we can get the id.
		console.log(req.params.courseId);



		// check thru req.body what the input is.
		console.log(req.body);

		// for update we use findByIdAndUpdate and has 3 arguments
		// {new:true} return the updated details

		let update = {
			name: req.body.name,
			description: req.body.description,
			price:req.body.price
		}

		Course.findByIdAndUpdate(req.params.courseId, update, {new:true})
		.then(result => res.send(result))
		.catch(error => res.send(error))
}


module.exports.archiveCourse = (req,res) => {
	console.log(req.params.courseId);

	let update = {
		isActive: false
	}
	Course.findByIdAndUpdate(req.params.courseId, update, {new:true})
	.then(result => res.send(result))
	.catch(error => res.send(error))
}
/*
	Naming convention should be named after the model/document ist is concerned.

*/


// Create Controller

// import the User model, brcypt, auth.js since we're now using it here.
const User = require("../models/User");
const Course = require("../models/Course");
const bcrypt = require("bcrypt");
const auth = require("../auth"); 

module.exports.registerUser = (req,res)=>{

	const hashedPw = bcrypt.hashSync(req.body.password,10);
	console.log(hashedPw);

	let newUser = new User({

		firstName: req.body.firstName,
		lastName: req.body.lastName,
		email: req.body.email,
		password: hashedPw,
		mobileNo: req.body.mobileNo

	})

	newUser.save()
	.then(result => res.send(result))
	.catch(error => res.send(error))

}


module.exports.getUserDetails = (req,res)=>{
	// console.log(req.user);
	let userId = req.user.id;
	// find({}) will find all matching documents
	//User.find({_id: userId})
	// findOne({}) will find one matching document only
	User.findOne({_id:userId})
	// from mongoose, findById() find a document by strictly its Id only.
	User.findById(userId)
	.then(result => res.send(result))
	.catch(error => res.send(error))

}

module.exports.loginUser = (req,res)=>{
	console.log(req.body);
	/*
		Steps
		1. find the user by its email
		2. if we found the user, check the password
		3. if we did'nt find, we will send a message
		4. if upon checking the password is the same from input, we will generate a key
		for the user to have authorization for user to access certain features in our app.

	*/
	User.findOne({email: req.body.email})
	.then(foundUser => {

		// foundUser is the parameter
		if(foundUser === null){
				return res.send({message: "No user found."});
		} else {
			// console.log(foundUser);

			// check the input if it matches
			// compareSync( input string, hashed String)
			const isPasswordCOrrect = bcrypt.compareSync(req.body.password, foundUser.password);
			//console.log(isPasswordCOrrect);
			if(isPasswordCOrrect){
				// create a key to authorized our user
				// we will creat our own module auth.js encoded string that contains our user details. This is what we call a JSONWebToken (JWT)
				return res.send({accessToken: auth.createAccessToken(foundUser)})

				//console.log("We will create a token for the user if the password is correct");
			} else{
				return res.send({message: "incorrect Password"})

			}
		}

	})

}

module.exports.checkUserEmail = (req, res) =>{

	User.findOne({email:req.body.email})
	.then(result => {

		if(result === null){
			return res.send(false);
		} else {
			return res.send(true)
		}

	})
	.catch(error => res.send(error))


}
module.exports.enroll = async(req,res) =>{

	// check the id of the user wgo will enroll
	//console.log(req.user.id);

	// check the id of the course we want to enroll
	//console.log(req.body.courseId);

	// validate the user if they are an admin or not.
	if(req.user.isAdmin){
		return res.send({message: "Action Forbidden"});
	}

	// async and await is added to our fucntion to make our function asynchronous. Means
	// that instead of js regular behavior of running each code line by line we will be able to
	// wait for the result of a function.

	let isUserUpdated = await User.findById(req.user.id).then(user => {

		// console.log(user);

		// add thye courseId in an object into the user's enrollemnt
		let newEnrollment = {
			courseId: req.body.courseId
		}

		// access the enrollemnt array and push another enrollment

		user.enrollments.push(newEnrollment);

		// after push sabe the user's document and return the value.
		return user.save().then(user => true).catch(err => err.message)
	})

	if(isUserUpdated !== true){
		return res.send({message: isUserUpdated});
	}
	//console.log(isUserUpdated);

	// find the course wehre we will enroll or add the user as an enrolle and return true if
	// we were able to push the user into the enrollees array properly or send erroe message instead

	let isCourseUpdated = await Course.findById(req.body.courseId).then(course => {
		// console.log(course);


		// create an object to push into the doocument array, enrollees
		let enrollee = {
			userId: req.user.id
		}
		course.enrollees.push(enrollee);
		return course.save().then(course => true).catch(err => err.message)


	})

	console.log(isCourseUpdated);

	if(isCourseUpdated !== true){
		return res.send({message: isCourseUpdated});
	}

	if(isUserUpdated && isCourseUpdated){
		return res.send({message: "Thank you for enrolling!"})
	}
}